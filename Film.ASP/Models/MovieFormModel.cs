﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Film.ASP.Models
{
    public class MovieFormModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
        public string Synopsis { get; set; }

        //[NotBefore1894]
        public DateTime? ReleaseDate { get; set; }

        public IFormFile File { get; set; }

        public byte[] Poster { get; set; }

        public string MimeType { get; set; }
    }
}
