﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Film.ASP.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Film.ASP.Controllers
{
    public class MovieController : Controller
    {
        private IConfiguration _config;
        public MovieController(IConfiguration configuration)
        {
            _config = configuration;
        }

        public IActionResult Index()
        {
            List<MovieModel> model = new List<MovieModel>();
            using (SqlConnection conn = new SqlConnection(_config.GetConnectionString("default")))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM Movie";
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    string blob = null;
                    if(reader["Poster"] != DBNull.Value)
                    {
                        byte[] poster = reader["Poster"] as byte[];
                        string b64 = Convert.ToBase64String(poster);
                        blob = $"data:{reader["MimeType"]};base64,{b64}";
                    }
                    model.Add(new MovieModel { 
                        Title = reader["Title"].ToString(),
                        Blob = blob
                    });
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(MovieFormModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.File != null)
                {
                    model.MimeType = model.File.ContentType;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        model.File.CopyTo(ms);
                        model.Poster = ms.ToArray();
                    }
                }
                //sauvegarder en db
                using (SqlConnection conn = new SqlConnection(_config.GetConnectionString("default")))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "INSERT INTO Movie(Title, Synopsis, ReleaseDate, Poster, MimeType)";
                    cmd.CommandText += " OUTPUT INSERTED.MovieId ";
                    cmd.CommandText += "VALUES (@p1,@p2,@p3,@p4,@p5)";
                    cmd.Parameters.AddWithValue("p1", model.Title);
                    cmd.Parameters.AddWithValue("p2", (object)model.Synopsis ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("p3", (object)model.ReleaseDate ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("p4", (object)model.Poster ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("p5", (object)model.MimeType ?? DBNull.Value);
                    int id = (int)cmd.ExecuteScalar();
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
    }
}
